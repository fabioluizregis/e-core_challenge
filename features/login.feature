Feature: Login
    Having the intention to validate if a user can login into the application
    As an user that has valid and invalid entries
    I want to validate the behaviour for valid and invalid credentials.

    @TC001 @positive_login
    Scenario Outline: Valid user authentication
        Given I access the main login page
        When I insert a <username> and <password>
        And I click on the login button
        Then I get to the Invoice List page

        Examples:
            | username   | password |
            | "demouser" | "abc123" | 


    @TC002 @negative_login
    Scenario Outline: Valid user authentication
        Given I access the main login page
        When I insert a <username> and <password>
        And I click on the login button
        Then I get to the message <message>

        Examples:
            | username    | password   | message                       |
            | "Demouser"  | "abc123"   | "Wrong username or password"  |
            | "demouser_" | "xyz"      | "Wrong username or password"  |
            | "demouser"  | "nananana" | "Wrong username or password"  |
            | "demouser"  | "abc123"   | "Wrong username or password"  |
