Feature: Invoice
Having the intention to validate all the information recorded into a invoice for a user
As an user that has access to the invoice page
I want to validate all the informatio recorded into my invoice.

@TC003 @validate_invoice_information
  Scenario Outline: Valid user authentication
    Given I access the Invoice List page using <username> and <password>
     When I get to the Invoice Details page related to <hotel_name>
     Then the information regarding to <hotel_name><invoice_date><due_date><invoice_number><booking_code><customer_details><room><checkin><checkout><total_stay_count><total_stay_amount><deposit_now><tax_vat><total_amount> are correct
  
    Examples: 
      | username   | password | hotel_name         | invoice_date | due_date     | invoice_number | booking_code | customer_details                         | room              | checkin      | checkout     | total_stay_count | total_stay_amount | deposit_now  | tax_vat      | total_amount  | 
      | "demouser" | "abc123" | "Rendezvous Hotel" | "14/01/2018" | "15/01/2018" | "110"          | "0875"       | "JOHNY SMITH R2, AVENUE DU MAROC 123456" | "Superior Double" | "14/01/2018" | "15/01/2018" | "1"              | "$150"            | "USD $20.90" | "USD $19.00" | "USD $209.00" | 

