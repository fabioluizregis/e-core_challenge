Given('I access the main login page') do
    visit '/'
end
  
When('I insert a {string} and {string}') do |username, password|

    @login.fill_login_fields(username, password)

end
  
When('I click on the login button') do
    @login.login_button.click
end
  
Then('I get to the Invoice List page') do
    expect(page).to have_content "Invoice List"
end
  
Then('I get to the message {string}') do |mensagem|
    expect(@login.alert_message.text).to have_content mensagem
end