Given('I access the Invoice List page using {string} and {string}') do |username, password|
    #steps %(I access the main login page)
    visit '/'
    @login.login(username, password)
    expect(@invoices_page.page_tittle.text).to have_content "Automation Example"
    expect(@invoices_page.table_tittle.text).to have_content "Invoice List"

end

When('I get to the Invoice Details page related to {string}') do |hotel_name|
    @invoices_page.access_invoice_details
    @details_page.switch_tab

    expect(@details_page.hotel_name.text).to have_content hotel_name
end
  
Then('the information regarding to {string}{string}{string}{string}{string}{string}{string}{string}{string}{string}{string}{string}{string}{string} are correct') do |hotel_name, invoice_date, due_date, invoice_number, booking_code, customer_details, room, checkin, checkout, total_stay_count, total_stay_amount, deposit_now, tax_vat, total_amount|
    expect(@details_page.hotel_name.text).to have_content hotel_name
    
    invoice_due_dates =  @details_page.dates_list[0].text.split("\n")
    expect(invoice_due_dates[0]).to have_content invoice_date
    expect(invoice_due_dates[1]).to have_content due_date

    expect(@details_page.invoice_number.text).to have_content invoice_number

    booking_details_info = @details_page.booking_details[0].text.split("\n")
    expect(booking_details_info[0]).to have_content booking_code

    customer_details_info = @details_page.customer_details[0].text.gsub("\n", " ")
    expect(customer_details_info).to have_content customer_details
    
    expect(booking_details_info[1]).to have_content room
    expect(booking_details_info[4]).to have_content checkin
    expect(booking_details_info[5]).to have_content checkout
    expect(booking_details_info[2]).to have_content total_stay_count
    expect(booking_details_info[3]).to have_content total_stay_amount

    expect(@details_page.deposit_now_details.text).to have_content deposit_now
    expect(@details_page.tax_vat_details.text).to have_content deposit_now
    expect(@details_page.total_amount_details.text).to have_content deposit_now

end