class DetailsPage < SitePrism::Page

    element :hotel_name, '.container h4[class="mt-5"]'
    element :invoice_number, 'h6[class="mt-2"]'
    elements :dates_list, '.container ul'
    elements :booking_details, '.table'
    elements :customer_details, '.container div'
    element :deposit_now_details, :xpath, '//table[2]/descendant::tbody/tr/td[1]'
    element :tax_vat_details, :xpath, '//table[2]/descendant::tbody/tr/td[2]'
    element :total_amount_details, :xpath, '//table[2]/descendant::tbody/tr/td[3]'

    def switch_tab()
        window = page.driver.browser.window_handles
        page.driver.browser.switch_to.window(window.last)
    end

end