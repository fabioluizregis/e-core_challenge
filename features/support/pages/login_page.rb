class LoginPage < SitePrism::Page

    element :username_field, 'input[name="username"]'
    element :password_field, 'input[name="password"]'
    element :login_button, '#btnLogin'
    element :alert_message, '.alert-danger'


    def fill_login_fields(username, password)
        username_field.set username
        password_field.set password
    end

    def login(username, password)
        username_field.set username
        password_field.set password
        login_button.click
    end
end