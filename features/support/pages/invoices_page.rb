class InvoicesPage < SitePrism::Page

    element :invoice_details, 'a[href="/invoice/0"]'
    element :page_tittle, '.navbar-brand'
    element :table_tittle, '.mt-5'

    def access_invoice_details()
        invoice_details.click
    end

end