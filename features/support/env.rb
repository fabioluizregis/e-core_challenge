require 'capybara'
require 'capybara/cucumber'
require 'site_prism'
require 'pry'

Capybara.configure do |config|
    config.default_driver = :selenium_chrome
    config.app_host = 'https://automation-sandbox.herokuapp.com'
end

Capybara.default_max_wait_time = 10