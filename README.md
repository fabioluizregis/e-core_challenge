# E-Core_challenge

Frontend automation challenge for E-Core

## Getting started

First of all, you will need to configure your environment:

1 - Clone the project to your machine

2 - Install [Ruby 3.1.2.1](https://rubyinstaller.org/downloads/)

3 - Install [Mozilla Firefox](https://www.mozilla.org/pt-BR/firefox/new/)

4 - Install [Gekodriver](https://github.com/mozilla/geckodriver/releases) regarding the version of your Mozilla Firefox installed

5 - Install the Ruby Gem "bundler"
```
gem install bundler
```
6 - On the root directory of the project run the following command to install the gems:
```
bundle install
```
7 - At the root project directiory, type the following command:
```
cucumber
```

After that, the automation shall run and you will be able to see the results inside de folder log.
There will be a file called "report.html" that you can open in any browser.